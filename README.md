# Atreus Firmware

This is not a firmware

It is the export files of the layers to get the following layout with
[Chrysalis](https://github.com/keyboardio/Chrysalis) for the Atreus keyboard.

In the `Normal` folder, the Fx (function keys, F1, F2 etc) are aligned
with the normal layout (12345..)

In the `Plus` folder, the Fx keys are optimized as the numbers
(7531980246).

![Image](Atreus-Optimp.png)
